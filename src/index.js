import Phaser from "phaser";

//Game Scenes
import BootScene from "./scenes/BootScene";
import PreloadScene from "./scenes/PreloadScene";
import IntroScene from "./scenes/IntroScene";
import TownScene from "./scenes/TownScene";
import TestInventory from "./scenes/TestInvetory";
import TrainingScene from "./scenes/TrainingScene";

const config = {
  type: Phaser.AUTO,
  parent: "phaser-example",
  // width: 1280,
  // height: 640,
  width: 800,
  height: 600,
  // width: window.innerWidth,
  // height: window.innerHeight,
  physics: {
    default: "arcade",
    arcade: {
      debug: false,
    },
  },
  scene: [
    BootScene,
    PreloadScene,
    IntroScene,
    TownScene,
    TrainingScene,
    TestInventory,
  ],
  pixelArt: true,
  audio: {
    disableWebAudio: true,
  },
};

const game = new Phaser.Game(config);
