//Player Component
var player;
var character;
var destination = null;
var activatedSkills;
export function PlayerComponent(self, layer) {
  player = self.physics.add.sprite(450, 300, "pirate_m1").setInteractive();
  player.setCollideWorldBounds(true);
  self.input.enableDebug(player, 0xff00ff);

  spriteAnimation(self, "pirate_m1");

  //Camera follow player
  self.cameras.main.startFollow(player, false, 0.05, 0.05);
  self.cameras.main.setZoom(2);

  var name = self.add
    .text(Math.floor(player.x), Math.floor(player.y - 20), "Emman", {
      fontFamily: '"Roboto"',
      fontSize: 9,
    })
    .setOrigin(0.5);

  layer.on(
    "pointerdown",
    function (pointer) {
      const transformedPoint = self.cameras.main.getWorldPoint(
        pointer.x,
        pointer.y
      );

      if (pointer.rightButtonDown()) {
        self.physics.moveToObject(player, transformedPoint, 150);
        destination = { x: transformedPoint.x, y: transformedPoint.y };

        characterMovement(player, "pirate_m1");
      } else {
      }
    },
    self
  );

  //Player Updates
  self.events.on(
    "update",
    function (time, delta) {
      if (
        destination !== null &&
        player.body.hitTest(destination.x, destination.y)
      ) {
        player.setVelocity(0);
        player.body.stop();
        player.anims.stop("walk");
        destination = null;
      }

      name.x = Math.floor(player.x);
      name.y = Math.floor(player.y - 40);
    },
    self
  );
}

export function BigBoss(self) {
  var bigboss = self.physics.add.sprite(450, 300, "bigboss");
  spriteAnimation(self, "bigboss");

  var name = self.add
    .text(bigboss.x, bigboss.y - 30, "Knight", {
      color: "gold",
      fontFamily: '"Roboto"',
      fontSize: 8,
    })
    .setOrigin(0.5);

  self.time.addEvent({
    delay: 2000, // ms
    callback: function () {
      var enemeyvelocityX = Math.random() * (80 - -80) + -80;
      var enemeyvelocityY = Math.random() * (80 - -80) + -80;

      if (enemeyvelocityX < 0) {
        bigboss.anims.play("bigboss-walkleft");
      } else {
        bigboss.anims.play("bigboss-walkright");
      }

      bigboss.setVelocity(enemeyvelocityX, enemeyvelocityY);

      // this.tweens.add({
      //   targets: container,
      //   x: bigboss,
      //   y: bigboss,
      //   duration: 2000,
      //   repeat: -1,
      // });

      // container.x = bigboss;
      // container.y = enemeyvelocityY;

      // self.physics.add.collider(enemy, topLayer);
    },
    callbackScope: self,
    loop: true,
  });

  self.events.on(
    "update",
    function (time, delta) {
      name.x = Math.floor(bigboss.x);
      name.y = Math.floor(bigboss.y - 40);
    },
    self
  );
}

export function EnemyComponent(self, type, counts) {
  if (type == "bunny") {
    EnemyAnimationPatrol(self, "enemy", 9, 11, 21, 23, 33, 35, 45, 47);
  }

  if (type == "frog") {
    EnemyAnimationPatrol(self, "enemy", 57, 59, 69, 71, 81, 83, 93, 95);
  }

  var EnemyArray = [];
  for (var i = 0; i <= counts; i++) {
    var enemy = self.physics.add
      .sprite(
        Phaser.Math.Between(0, 1260),
        Phaser.Math.Between(0, 1260),
        "enemy",
        9
      )
      .setInteractive();

    enemy.setCollideWorldBounds(true);

    EnemyArray.push(enemy);
  }

  EnemyArray.map(function (val, key) {
    let enemyHP = 100;
    val.visible = false;
    let walkRandom = self.time.addEvent({
      delay: Phaser.Math.Between(500, 8000), // ms
      callback: function () {
        val.visible = true;
        var enemeyvelocityX = Phaser.Math.Between(-50, 50);
        var enemeyvelocityY = Phaser.Math.Between(-50, 50);

        if (enemeyvelocityX < 0) {
          if (enemeyvelocityX > -25 && enemeyvelocityY > 0) {
            val.anims.play("anim-down");
          } else {
            val.anims.play("anim-left");
          }
        } else {
          if (enemeyvelocityX < 25 && enemeyvelocityY < 0) {
            val.anims.play("anim-up");
          } else if (enemeyvelocityX < 25 && enemeyvelocityY > 0) {
            val.anims.play("anim-down");
          } else {
            val.anims.play("anim-right");
          }
        }

        val.setVelocity(enemeyvelocityX, enemeyvelocityY);

        // characterMovement(enemy);
      },
      callbackScope: self,
      loop: true,
    });

    val.on(
      "pointerdown",
      function (pointer) {
        if (!pointer.rightButtonDown()) {
          const transformedPoint = self.cameras.main.getWorldPoint(
            pointer.x,
            pointer.y
          );

          var music = self.sound.add("punch");
          music.play();

          self.physics.moveToObject(val, character, 150);

          self.time.addEvent({
            delay: 500, // ms
            callback: function () {
              self.physics.moveToObject(val, character, 80);
            },
            callbackScope: self,
            loop: true,
          });

          destination = { x: transformedPoint.x, y: transformedPoint.y };
          characterMovement(character, "player");

          var playerhits = Phaser.Math.Between(1, 10);
          enemyHP = enemyHP - playerhits;

          if (playerhits > 8) {
            var hipoints = self.add.text(val.x, val.y - 20, "-" + playerhits, {
              fontFamily: '"Roboto"',
              fontSize: 10,
              color: "red",
            });
          } else {
            var hipoints = self.add.text(val.x, val.y - 20, "-" + playerhits, {
              fontFamily: '"Roboto"',
              fontSize: 10,
            });
          }

          this.tweens.add({
            targets: hipoints,
            y: val.y,
            duration: 500,
            ease: "Power2",
            yoyo: true,
          });

          self.time.addEvent({
            delay: 500, // ms
            callback: function () {
              hipoints.destroy();
            },
            callbackScope: self,
            loop: true,
          });

          if (enemyHP <= 0) {
            var itemtest = self.add
              .image(val.x, val.y, "item_dagger")
              .setScale(0.5);
            this.tweens.add({
              targets: itemtest,
              y: val.y - 30,
              duration: 500,
              ease: "Power2",
              yoyo: true,
            });

            val.destroy();
            walkRandom.remove();
          }
        }
      },
      self
    );

    self.events.on(
      "update",
      function (time, delta) {
        if (
          destination !== null &&
          character.body.hitTest(destination.x, destination.y)
        ) {
          character.setVelocity(0);
          character.body.stop();
          character.anims.stop("walk");
          destination = null;
        }

        // name.x = Math.floor(player.x);
        // name.y = Math.floor(player.y - 40);
      },
      self
    );
  });
}

function EnemyAnimationPatrol(self, key, d1, d2, l1, l2, r1, r2, u1, u2) {
  self.anims.remove("anim-up");
  self.anims.remove("anim-down");
  self.anims.remove("anim-left");
  self.anims.remove("anim-right");

  self.anims.create({
    key: "anim-down",
    frames: self.anims.generateFrameNumbers(key, {
      start: d1,
      end: d2,
    }),
    frameRate: 5,
    repeat: -1,
  });

  self.anims.create({
    key: "anim-left",
    frames: self.anims.generateFrameNumbers(key, {
      start: l1,
      end: l2,
    }),
    frameRate: 5,
    repeat: -1,
  });

  self.anims.create({
    key: "anim-right",
    frames: self.anims.generateFrameNumbers(key, {
      start: r1,
      end: r2,
    }),
    frameRate: 5,
    repeat: -1,
  });

  self.anims.create({
    key: "anim-up",
    frames: self.anims.generateFrameNumbers(key, {
      start: u1,
      end: u2,
    }),
    frameRate: 5,
    repeat: -1,
  });
}

//Pet Component
export function PetComponent(self, player) {
  var pet = self.physics.add.sprite(
    Phaser.Math.Between(player.x, player.x + 200),
    Phaser.Math.Between(player.y, player.y + 200),
    "phoenix"
  );
  var pet1 = self.physics.add.sprite(
    Phaser.Math.Between(player.x, player.x + 200),
    Phaser.Math.Between(player.y, player.y + 200),
    "leviathan"
  );

  var pet2 = self.physics.add.sprite(
    Phaser.Math.Between(player.x, player.x + 200),
    Phaser.Math.Between(player.y, player.y + 200),
    "bahamut"
  );

  pet.setCollideWorldBounds(true);

  spriteAnimation(self, "phoenix");
  spriteAnimation(self, "leviathan");
  spriteAnimation(self, "bahamut");

  self.time.addEvent({
    delay: Phaser.Math.Between(2000, 3000), // ms
    callback: function () {
      // Character Movement logic
      if (pet2.x > player.x) {
        pet2.anims.play("bahamut-walkleft");
      } else {
        pet2.anims.play("bahamut-walkright");
      }

      self.physics.moveTo(pet2, player.x, player.y, 100);
    },
    callbackScope: self,
    loop: true,
  });

  self.time.addEvent({
    delay: Phaser.Math.Between(2000, 3000), // ms
    callback: function () {
      // Character Movement logic
      if (pet1.x > player.x) {
        pet1.anims.play("leviathan-walkleft");
      } else {
        pet1.anims.play("leviathan-walkright");
      }

      self.physics.moveTo(pet1, player.x, player.y, 100);
    },
    callbackScope: self,
    loop: true,
  });

  self.time.addEvent({
    delay: Phaser.Math.Between(2000, 3000), // ms
    callback: function () {
      // Character Movement logic
      if (pet.x > player.x) {
        pet.anims.play("phoenix-walkleft");
      } else {
        pet.anims.play("phoenix-walkright");
      }

      self.physics.moveTo(pet, player.x, player.y, 100);
    },
    callbackScope: self,
    loop: true,
  });
}

export function MouseUI(self, layer, c1, c2) {
  layer.on(
    "pointerdown",
    function (pointer) {
      if (pointer.rightButtonDown()) {
        self.input.setDefaultCursor("url(" + c2 + "), pointer");
      } else {
      }
    },
    self
  );

  layer.on(
    "pointerup",
    function (pointer) {
      self.input.setDefaultCursor("url(" + c1 + "), pointer");
    },
    self
  );
}

export function spriteAnimation(self, key) {
  self.anims.create({
    key: key + "-walkup",
    frames: self.anims.generateFrameNumbers(key, {
      start: 12,
      end: 15,
    }),
    frameRate: 5,
    repeat: -1,
  });

  self.anims.create({
    key: key + "-walkdown",
    frames: self.anims.generateFrameNumbers(key, {
      start: 0,
      end: 3,
    }),
    frameRate: 5,
    repeat: -1,
  });

  self.anims.create({
    key: key + "-walkleft",
    frames: self.anims.generateFrameNumbers(key, {
      start: 4,
      end: 7,
    }),
    frameRate: 5,
    repeat: -1,
  });

  self.anims.create({
    key: key + "-walkright",
    frames: self.anims.generateFrameNumbers(key, {
      start: 8,
      end: 11,
    }),
    frameRate: 5,
    repeat: -1,
  });
}

export function characterMovement(char, key) {
  if (char.body.velocity.x < 0) {
    if (char.body.velocity.x > -20 && char.body.velocity.y < 0) {
      char.anims.play(key + "-walkup");
    } else if (char.body.velocity.x > -20 && char.body.velocity.y > 0) {
      char.anims.play(key + "-walkdown");
    } else {
      char.anims.play(key + "-walkleft");
    }
  } else {
    if (char.body.velocity.x < 20 && char.body.velocity.y < 0) {
      char.anims.play(key + "-walkup");
    } else if (char.body.velocity.x < 20 && char.body.velocity.y > 0) {
      char.anims.play(key + "-walkdown");
    } else {
      char.anims.play(key + "-walkright");
    }
  }
}

export function enemyPatrol(self, x, y, name, sprite_key) {
  var self = self;

  var enemy = self.physics.add.sprite(x, y, sprite_key).setInteractive();

  spriteAnimation(self, sprite_key);

  // self.input.enableDebug(enemy, 0xff00ff);

  enemy.setCollideWorldBounds(true);

  var name = self.add
    .text(enemy.x, enemy.y - 30, name, {
      fontFamily: '"Roboto"',
      fontSize: 8,
    })
    .setOrigin(0.5);
  var hpgauge = 100;

  // var graphics = self.add.graphics();
  // var line1 = new Phaser.Geom.Line(
  //   enemy.x,
  //   enemy.y - 25,
  //   enemy.x + 50,
  //   enemy.y - 25
  // );
  // graphics.lineStyle(2, 0x00ff00);
  // graphics.strokeLineShape(line1);

  var hp1 = self.add
    .text(enemy.x, enemy.y - 25, "hp:" + hpgauge, {
      fontFamily: '"Roboto"',
      fontSize: 8,
    })
    .setOrigin(0.5);

  // enemy.setCollideWorldBounds(true);

  // enemy.setScale(0.8);
  // enemy.anims.delayedPlay(Math.random() * 3, "enemywalk");

  // self.physics.add.collider(
  //   player,
  //   enemy,
  //   null,
  //   function () {
  //     // alert("attacking");
  //   },
  //   this
  // );

  var spellstest = {
    key: "tests",
    frames: self.anims.generateFrameNumbers("s3", {
      start: 0,
      end: 16,
      // frames: [0, 4],
    }),
    frameRate: 10,
    repeat: 1,
  };
  self.anims.create(spellstest);

  var killEnemy = false;
  enemy.on("pointerdown", function () {
    killEnemy = true;
    var music = self.sound.add("punch");

    var speels = self.add.sprite(enemy.x, enemy.y, "s3");
    speels.anims.play("tests");
    self.time.addEvent({
      delay: 1500, // ms
      callback: function () {
        speels.destroy();
      },
      callbackScope: self,
      loop: false,
    });

    music.play();
    // self.physics.moveToObject(player, enemy, 150);
    hpgauge = hpgauge - 5;
    // music.stop();
    // enemy.destroy();
  });

  self.events.on(
    "update",
    function (time, delta) {
      name.x = Math.floor(enemy.x);
      name.y = Math.floor(enemy.y - 30);
      hp1.x = Math.floor(enemy.x);
      hp1.y = Math.floor(enemy.y - 25);
      hp1.text = "hp:" + hpgauge;
    },
    self
  );

  if (killEnemy) {
    self.physics.add.collider(
      activatedSkills,
      enemy,
      null,
      function () {
        enemy.destroy();
      },
      this
    );
  }

  self.time.addEvent({
    delay: 2000, // ms
    callback: function () {
      var enemeyvelocityX = Math.random() * (80 - -80) + -80;
      var enemeyvelocityY = Math.random() * (80 - -80) + -80;

      if (enemeyvelocityX < 0) {
        enemy.anims.play(sprite_key + "-walkleft");
      } else {
        enemy.anims.play(sprite_key + "-walkright");
      }

      enemy.setVelocity(enemeyvelocityX, enemeyvelocityY);
      characterMovement(enemy, sprite_key);

      // this.tweens.add({
      //   targets: container,
      //   x: enemeyvelocityX,
      //   y: enemeyvelocityX,
      //   duration: 2000,
      //   repeat: -1,
      // });

      // container.x = enemeyvelocityX;
      // container.y = enemeyvelocityY;

      // self.physics.add.collider(enemy, topLayer);
    },
    callbackScope: self,
    loop: true,
  });
}

export function setGameUI(self) {
  var container1;
  var uielements = self.add
    .image(0, 0, "health_bar")
    .setOrigin(0)
    .setScrollFactor(0);

  var mapsettings = self.add
    .image(self.game.config.width / 2 + 250, 0, "map_ui")
    .setOrigin(0)
    .setInteractive()
    .setScrollFactor(0);

  mapsettings.on(
    "pointerup",
    function () {
      // self.scene.switch("testinventory-scene");

      var isActive = self.scene.isActive("testinventory-scene");
      var isVisible = self.scene.isVisible("testinventory-scene");

      if (isActive && isVisible) {
        self.scene.stop("testinventory-scene");
      } else {
        self.scene.run("testinventory-scene", {
          id: 1,
          image: "babar-phaleon-coco.png",
        });
      }
    },
    self
  );
}

export function questBoxUi(self) {
  // var skillBar = self.add
  //   .image(100, 400, "quest_box")
  //   .setInteractive()
  //   .setOrigin(0.5);
  // // skillBar.on("pointerdown", function () {
  // var quest_board = self.add
  //   .image(self.game.config.width / 2, self.game.config.height / 2, "questui")
  //   .setOrigin(1)
  //   .setScrollFactor(0);
  // var x = 75;
  // var y = 30;
  // var name = self.add
  //   .text(quest_board.x / 2, quest_board.y / 2, "Main Quests", {
  //     fontFamily: '"Alagard"',
  //     fontSize: 20,
  //   })
  //   .setScrollFactor(0)
  //   .setOrigin(1);
  // for (var i = 0; i < 4; i++) {
  //   var skillBar = self.add
  //     .image(quest_board.x - 73, quest_board.y - y, "questuioptions")
  //     .setInteractive();
  //   y = y - 43;
  // }
  // });
}

export function skillBarUi(self, player, ground, camera) {
  var skillBar = self.add
    .image(
      self.game.config.width / 2,
      self.game.config.height - 300,
      "skill_bar"
    )
    .setScrollFactor(0);

  var d = 100;
  var spellsArray = [];
  for (var i = 1; i <= 3; i++) {
    var spells = self.add
      .image(
        self.game.config.width / 2 + d - 144,
        self.game.config.height - 300,
        "spell" + i
      )
      .setInteractive()
      .setScale(0.3)
      .setScrollFactor(0);

    var offset = spells.getTopLeft();
    d = d + 45;
    spellsArray.push(spells);
  }
  if (spellsArray.length !== 0) {
    spellsArray.map(function (val, key) {
      if (key != 2) {
        val.on("pointerdown", function (pointer) {
          var shape1 = new Phaser.Geom.Circle(0, 0, 50);
          var shape2 = new Phaser.Geom.Ellipse(0, 0, 40, 45);
          var shape3 = new Phaser.Geom.Rectangle(-150, -150, 300, 300);
          var shape4 = new Phaser.Geom.Line(0, 0, 20, 0);
          var shape5 = new Phaser.Geom.Triangle.BuildEquilateral(0, -140, 300);
          var shapes = [shape1, shape2, shape4, shape4, shape5];

          var blueemitter = self.add.particles("blue_emitter");
          var redemitter = self.add.particles("red_emitter");

          var emitter1 = redemitter.createEmitter({
            // x: player.x,
            // y: player.y,
            angle: { min: 180, max: 360 },
            speed: 1,
            lifespan: 500,
            scale: { start: 0.2, end: 0 },
            emitZone: {
              type: "edge",
              source: shapes[key + 1],
              quantity: 49,
              yoyo: false,
            },
            blendMode: "ADD",
          });

          var emitter = blueemitter.createEmitter({
            // x: player.x,
            // y: player.y,
            angle: { min: 180, max: 360 },
            speed: 1,
            lifespan: 300,
            scale: { start: 0.2, end: 0 },
            emitZone: {
              type: "edge",
              source: shapes[key],
              quantity: 50,
              yoyo: false,
            },
            blendMode: "ADD",
          });

          emitter.startFollow(player);
          emitter1.startFollow(player);

          // self.time.addEvent({
          //   delay: 10000, // ms
          //   callback: function () {
          //     blueemitter.destroy();
          //     redemitter.destroy();
          //   },
          //   callbackScope: self,
          //   loop: false,
          // });
        });
      } else {
        var attackSpell = false;
        var attackEmitter;

        if (!attackSpell) {
          val.on("pointerdown", function () {
            attackSpell = true;
            attackEmitter = self.add.particles("red_emitter");

            var shape3 = new Phaser.Geom.Rectangle(
              self.game.config.width / 2 + 27,
              self.game.config.height - 315,
              33,
              33
            );
            attackEmitter
              .createEmitter({
                angle: { min: 180, max: 360 },
                speed: 1,
                lifespan: 500,
                scale: { start: 0.1, end: 0 },
                emitZone: {
                  type: "edge",
                  source: shape3,
                  quantity: 49,
                  yoyo: false,
                },
                blendMode: "ADD",
              })
              .setScrollFactor(0);
          });
        }

        ground.on("pointerdown", function (pointer) {
          if (!pointer.rightButtonDown()) {
            const transformedPoint = camera.main.getWorldPoint(
              pointer.x,
              pointer.y
            );
            var shape4 = new Phaser.Geom.Line(
              0,
              0,
              transformedPoint.x - player.x,
              transformedPoint.y - player.y
            );

            if (attackSpell) {
              // var emitter = particles.createEmitter({
              //   x: player.x,
              //   y: player.y,
              //   angle: { min: 180, max: 360 },
              //   speed: 50,
              //   gravityX: 0,
              //   gravityY: 100,
              //   lifespan: 800,
              //   scale: { start: 0.2, end: 0 },
              //   emitZone: {
              //     type: "edge",
              //     source: shape4,
              //     quantity: 60,
              //     yoyo: false,
              //   },
              //   blendMode: "ADD",
              // });
              // self.time.addEvent({
              //   delay: 1000, // ms
              //   callback: function () {
              //     particles.destroy();
              //   },
              //   callbackScope: self,
              //   loop: false,
              // });
              // self.time.addEvent({
              //   delay: 10000, // ms
              //   callback: function () {
              //     attackEmitter.destroy();
              //     attackSpell = false;
              //   },
              //   callbackScope: self,
              //   loop: false,
              // });
            }
          } else {
            if (pointer.getDuration() > 500) {
            } else {
            }
          }
        });
      }
    });
  }
}

export function SpellsTesting(self, ground) {
  var interval = 300;

  var spellsArray = [
    "1. Fire Lion",
    "2. Ice Shield",
    "3. Ice Tacle",
    "4. Lightning Claw",
    "5. Summon Snake",
    "6. Spikes",
    "7. Tornado",
    "8. Torrent Tacle",
    "9. Turtle Shell",
  ];

  var spellsObjArray = [];
  var textgap = 300;
  for (var i = 0; i < spellsArray.length; i++) {
    var spell1 = self.add
      .text(0, textgap, spellsArray[i], {
        fontFamily: '"Alagard"',
        fontSize: 20,
      })
      .setInteractive()
      .setScrollFactor(0);

    hoverEffect(spell1);

    textgap = textgap + 20;
    spellsObjArray.push(spell1);
  }
  var spellsKey = "s0";
  spellsObjArray.map(function (val, key) {
    val.on("pointerdown", function () {
      spellsKey = "s" + key;
      activatedSkills = val;
    });
  });

  ////////////////////
  ground.on("pointerdown", function (pointer) {
    if (!pointer.rightButtonDown()) {
      const transformedPoint = self.cameras.main.getWorldPoint(
        pointer.x,
        pointer.y
      );

      var spellstest = {
        key: "tests" + spellsKey,
        frames: self.anims.generateFrameNumbers(spellsKey, {
          start: 0,
          end: 16,
          // frames: [0, 4],
        }),
        frameRate: 10,
        repeat: -1,
      };

      self.anims.create(spellstest);

      var d1 = transformedPoint.x;
      var d2 = transformedPoint.y;
      if (spellsKey === "s1" || spellsKey === "s8") {
        d1 = character.x + 5;
        d2 = character.y;
      }
      // var music = self.sound.add("katon");

      // music.play();
      self.time.addEvent({
        delay: 500, // ms
        callback: function () {
          var speels = self.add.sprite(d1, d2, spellsKey);
          speels.anims.play("tests" + spellsKey);

          self.time.addEvent({
            delay: 1500, // ms
            callback: function () {
              speels.destroy();
            },
            callbackScope: self,
            loop: false,
          });
        },
        callbackScope: self,
        loop: false,
      });

      // speels.setCollideWorldBounds(true);
    }
  });
}

function hoverEffect(object) {
  object.on("pointerover", function () {
    object.setColor("#000");
  });

  object.on("pointerout", function () {
    object.setColor("#fff");
  });
}

///////////////////////PLAYERCOMPONENT

export function CharacterComponent(self, ground_layer, sprite_key) {
  //Setting Character sprite
  character = self.physics.add.sprite(450, 300, sprite_key).setInteractive();
  character.setCollideWorldBounds(true);
  self.input.enableDebug(character, 0xff00ff);

  PetComponent(self, character);

  //Sprites walking animation init
  spriteAnimation(self, sprite_key);

  //Camera follow character
  self.cameras.main.startFollow(character, false, 0.05, 0.05);
  // self.cameras.main.setZoom(2);

  var character_name = self.add
    .text(Math.floor(character.x), Math.floor(character.y - 20), "Emman", {
      fontFamily: '"Roboto"',
      fontSize: 9,
    })
    .setOrigin(0.5);

  ground_layer.on(
    "pointerdown",
    function (pointer) {
      const transformedPoint = self.cameras.main.getWorldPoint(
        pointer.x,
        pointer.y
      );

      if (pointer.rightButtonDown()) {
        self.physics.moveToObject(character, transformedPoint, 150);
        destination = { x: transformedPoint.x, y: transformedPoint.y };

        // Character Movement logic
        if (character.body.velocity.x < 0) {
          if (
            character.body.velocity.x > -20 &&
            character.body.velocity.y < 0
          ) {
            character.anims.play(sprite_key + "-walkup");
          } else if (
            character.body.velocity.x > -20 &&
            character.body.velocity.y > 0
          ) {
            character.anims.play(sprite_key + "-walkdown");
          } else {
            character.anims.play(sprite_key + "-walkleft");
          }
        } else {
          if (character.body.velocity.x < 20 && character.body.velocity.y < 0) {
            character.anims.play(sprite_key + "-walkup");
          } else if (
            character.body.velocity.x < 20 &&
            character.body.velocity.y > 0
          ) {
            character.anims.play(sprite_key + "-walkdown");
          } else {
            character.anims.play(sprite_key + "-walkright");
          }
        }
      } else {
      }
    },
    self
  );

  //Character Update Event
  self.events.on(
    "update",
    function (time, delta) {
      if (
        destination !== null &&
        character.body.hitTest(destination.x, destination.y)
      ) {
        character.setVelocity(0);
        character.body.stop();
        character.anims.stop("walk");
        destination = null;
      }

      character_name.x = Math.floor(character.x);
      character_name.y = Math.floor(character.y - 40);
    },
    self
  );
}
