import { Scene } from "phaser";

class BootScene extends Scene {
  create() {
    this.scene.start("preload-scene");
  }
}

export default BootScene;
