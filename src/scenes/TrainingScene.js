import { Scene } from "phaser";

import {
  characterMovement,
  enemyPatrol,
  setGameUI,
  PlayerComponent,
  EnemyComponent,
  questBoxUi,
  BigBoss,
  CharacterComponent,
  SpellsTesting,
} from "../functions/Global";

var player;
var enemy;
var cursors;
var destination = null;
var text;
var enemyDoneWalking = true;
var customenemy;

import NamesArr from "../assets/name.json";

class TrainingScene extends Scene {
  constructor() {
    super("training-scene");
  }

  create() {
    this.cameras.main.setBackgroundColor("#ddd");

    let mappy = this.add.tilemap("trainingground");

    this.cameras.main.setBounds(
      0,
      0,
      mappy.widthInPixels,
      mappy.heightInPixels
    );
    this.physics.world.setBounds(
      0,
      0,
      mappy.widthInPixels,
      mappy.heightInPixels
    );

    let terrain = mappy.addTilesetImage("terrain_atlas", "terrain_atlas");

    let botLayer = mappy
      .createStaticLayer("Ground", [terrain], 0, 0)
      .setInteractive()
      .setDepth(-1);
    let topLayer = mappy.createStaticLayer("Plants", [terrain], 0, 0);

    //map collisions
    //by tile property
    topLayer.setCollisionByProperty({ collides: true });

    var gototraining = this.add.image(400, 400, "goto-town").setInteractive();
    // .setScrollFactor(0);

    gototraining.once(
      "pointerup",
      function () {
        // alert();
        this.scene.start("town-scene");
      },
      this
    );

    //by tile index
    topLayer.setCollision([269, 270, 271, 301, 302, 303, 333, 334, 335]);

    for (var i = 0; i < 10; i++) {
      var x = Phaser.Math.Between(0, 1200);
      var y = Phaser.Math.Between(0, 1200);
      var charArr = ["bigboss", "pirate_m1", "magratgarlick"];
      enemyPatrol(
        this,
        x,
        y,
        NamesArr.names[Phaser.Math.Between(0, 4949)],
        charArr[Phaser.Math.Between(0, 2)]
      );
    }

    // text = this.add
    //   .text(200, 100, "Cursors to move", {
    //     font: "14px Courier",
    //     fill: "#000",
    //   })
    //   .setScrollFactor(0);
    CharacterComponent(this, botLayer, "pirate_m1");
    EnemyComponent(this, "bunny", 20);
    BigBoss(this);
    // questBoxUi(this);
    SpellsTesting(this, botLayer);
    setGameUI(this);
  }

  update() {}
}

export default TrainingScene;
