import { Scene } from "phaser";

import {
  characterMovement,
  enemyPatrol,
  setGameUI,
  questBoxUi,
  skillBarUi,
  SpellsTesting,
  PlayerComponent,
  MouseUI,
  EnemyComponent,
  CharacterComponent,
} from "../functions/Global";

import cursorDefault from "../assets/cursor/tap.cur";
import cursorDefaultWalk from "../assets/cursor/tapTick.cur";

import NamesArr from "../assets/name.json";

var player;
var enemy;
var cursors;
var destination = null;
var text;
var enemyDoneWalking = true;
var bar;
var pet;
var name;

class TownScene extends Scene {
  constructor() {
    super("town-scene");
  }

  create() {
    this.scene.remove("preload-scene");
    var music = this.sound.add("theme");
    music.play();

    this.cameras.main.setBackgroundColor("#000");

    let mappy = this.add.tilemap("townplace");
    let mappy1 = this.add.tilemap("trainingground");

    this.cameras.main.setBounds(
      0,
      0,
      mappy.widthInPixels,
      mappy.heightInPixels
    );
    this.physics.world.setBounds(
      0,
      0,
      mappy.widthInPixels,
      mappy.heightInPixels
    );

    console.log(mappy);

    // this.cameras.main.setBounds(0, 0, 800 * 2, 600 * 2);
    // this.physics.world.setBounds(0, 0, 800 * 2, 0 * 2);

    let terrain1 = mappy.addTilesetImage("town_atlas", "town_atlas");
    let terrain2 = mappy1.addTilesetImage("terrain_atlas", "terrain_atlas");

    let botLayer = mappy
      .createStaticLayer("Ground", [terrain2], 0, 0)
      .setInteractive();
    // let botLayer1 = mappy1.createStaticLayer("Ground", [terrain2], 0, 0);
    // let topLayer = mappy.createStaticLayer("Buildings", [terrain1], 0, 0);
    // topLayer.setCollisionByProperty({ collides: true });

    // cursors = this.input.keyboard.createCursorKeys();

    // enemy = this.physics.add.sprite(450, 300, "enemy");
    // enemy.anims.play("enemywalk");

    var gototraining = this.add
      .image(100, 300, "goto-training")
      .setInteractive();

    gototraining.once(
      "pointerup",
      function () {
        // alert();
        this.scene.start("training-scene");
      },
      this
    );

    for (var i = 0; i < 5; i++) {
      var x = Phaser.Math.Between(0, 1260);
      var y = Phaser.Math.Between(0, 1260);
      var charArr = ["bigboss", "pirate_m1", "magratgarlick", "berserker_f"];
      enemyPatrol(
        this,
        x,
        y,
        NamesArr.names[Phaser.Math.Between(0, 4949)],
        charArr[Phaser.Math.Between(0, 3)]
      );
    }

    ///////////////////////

    // skillBarUi(this, player, botLayer, this.cameras);
    MouseUI(this, botLayer, cursorDefault, cursorDefaultWalk);

    CharacterComponent(this, botLayer, "berserker_f");
    // PlayerComponent(this, botLayer);
    // EnemyComponent(this, "frog", 10);
    // questBoxUi(this);
    setGameUI(this);

    // SpellsTesting(this, botLayer, player);

    //  A drop zone
    ////////////////////////////////////
  }

  update() {}
}

export default TownScene;
