import { Scene } from "phaser";

import ground from "../assets/sprites/bg.png";
// import player from "../assets/sprites/Player/p1_walk/p1_walk.png";
import player from "../assets/spritetest2.png";
import bigboss from "../assets/bigboss.png";
import enemy from "../assets/sprites/enemy_animals.png";

import terrain from "../assets/terrain_atlas.png";
import town from "../assets/town_atlas.png";

import trainingground from "../assets/tilemap/training_ground.json";
import townplace from "../assets/tilemap/town.json";
import testingGround from "../assets/tilemap/testing_ground.json";

import boxTrainingGround from "../assets/sprites/Tiles/signExit.png";
import boxTownPlace from "../assets/sprites/Tiles/sign.png";

//UI ELEMENTS HERE
import healthBar from "../assets/ui/health-bar.png";
import inventory from "../assets/ui/inventory.png";
import questui from "../assets/ui/quest-ui.png";
import questoptions from "../assets/ui/quest-options.png";
import mapUi from "../assets/ui/map-settings.png";
import skillBar from "../assets/ui/skill_bar.png";
import itemStorage from "../assets/ui/item_storage.png";

import townmusicOGG from "../assets/sounds/TownTheme.ogg";
import townmusicMP3 from "../assets/sounds/TownTheme.mp3";
import katonMP3 from "../assets/sounds/katon.mp3";

//hits
import punchOgg from "../assets/sounds/hits/17.ogg";

import introMusic from "../assets/sounds/intro_loop.wav";

import pet from "../assets/sprites/pet1.png";

import cursorDefault from "../assets/cursor/tap.cur";
import cursorDefaultWalk from "../assets/cursor/tapTick.cur";
import demowindow from "../assets/test-windo.png";

import item_axe from "../assets/items/envelope.png";
import item_dagger from "../assets/items/upg_bow.png";

import questBox from "../assets/sprites/Tiles/boxItemAlt.png";

import blueEmitter from "../assets/blue.png";
import redEmitter from "../assets/red.png";

import introBG from "../assets/bg_intro.png";

import spell1 from "../assets/spells/spell1.png";
import spell2 from "../assets/spells/spell2.png";
import spell3 from "../assets/spells/spell3.png";

//New spells animation
import spellsSprite from "../assets/spells/lightning1.png";

import s0 from "../assets/spells/sheets/firelion_down.png";
import s1 from "../assets/spells/sheets/iceshield.png";
import s2 from "../assets/spells/sheets/icetacle.png";
import s3 from "../assets/spells/sheets/lightningclaw.png";
import s4 from "../assets/spells/sheets/snakebite_side.png";
import s5 from "../assets/spells/sheets/spikes.png";
import s6 from "../assets/spells/sheets/tornado.png";
import s7 from "../assets/spells/sheets/torrentacle.png";
import s8 from "../assets/spells/sheets/turtleshell_front.png";

import sprite_atlas from "../assets/atlas/newatlas.png";
import json_atlas from "../assets/atlas/newatlas.json";

class PreloadScene extends Scene {
  constructor() {
    super("preload-scene");
  }

  preload() {
    // Called first, used to load assets
    this.load.image("bg", ground);

    let loadingBar = this.add.graphics({
      fillStyle: {
        color: 0xffffff,
      },
    });

    this.load.on("progress", (percent) => {
      loadingBar.fillRect(
        0,
        this.game.renderer.height / 2,
        this.game.renderer.width * percent,
        50
      );
    });

    // this.load.spritesheet("sprite_atlas", sprite_atlas);

    this.load.atlas("json_atlas", sprite_atlas, json_atlas);

    // this.load.spritesheet("player", player, {
    //   frameWidth: 50,
    //   frameHeight: 85,
    // });

    // this.load.spritesheet("bigboss", bigboss, {
    //   frameWidth: 32,
    //   frameHeight: 48,
    // });

    this.load.spritesheet("enemy", enemy, {
      frameWidth: 32,
      frameHeight: 32,
    });

    this.load.spritesheet("pet", pet, {
      frameWidth: 100,
      frameHeight: 100,
    });

    this.load.spritesheet("spells_sprite", spellsSprite, {
      frameWidth: 50,
      frameHeight: 100,
    });

    var sArr = [s0, s1, s2, s3, s4, s5, s6, s7, s8];

    for (var i = 0; i < sArr.length; i++) {
      this.load.spritesheet("s" + i, sArr[i], {
        frameWidth: 128,
        frameHeight: 128,
      });
    }

    this.load.image("terrain_atlas", terrain);
    this.load.image("town_atlas", town);

    this.load.image("item_axe", item_axe);
    this.load.image("item_dagger", item_dagger);
    this.load.image("item_storage", itemStorage);

    //LOAD UI HERE
    this.load.image("health_bar", healthBar);
    this.load.image("inventory", inventory);
    this.load.image("questui", questui);
    this.load.image("questuioptions", questoptions);
    this.load.image("map_ui", mapUi);
    this.load.image("skill_bar", skillBar);

    this.load.tilemapTiledJSON("trainingground", trainingground);
    this.load.tilemapTiledJSON("townplace", townplace);
    this.load.tilemapTiledJSON("test_place", testingGround);
    // this.load.tilemapTiledJSON("trainingground", trainingground);

    this.load.image("goto-town", boxTownPlace);
    this.load.image("goto-training", boxTrainingGround);

    this.load.audio("theme", [townmusicOGG, townmusicMP3]);
    this.load.audio("intro", [introMusic]);
    this.load.audio("katon", [katonMP3]);
    this.load.audio("punch", [punchOgg]);

    this.load.image("demowindow", demowindow);
    this.load.image("quest_box", questBox);
    this.load.image("blue_emitter", blueEmitter);
    this.load.image("red_emitter", redEmitter);

    this.load.image("intro_bg", introBG);

    //spells

    this.load.image("spell1", spell1);
    this.load.image("spell2", spell2);
    this.load.image("spell3", spell3);
  }

  create() {
    // this.scene.start("intro-scene");

    this.input.setDefaultCursor("url(" + cursorDefault + "), pointer");
    this.input.mouse.disableContextMenu();

    this.textures.addSpriteSheetFromAtlas("pirate_m1", {
      frameWidth: 32,
      frameHeight: 48,
      atlas: "json_atlas",
      frame: "pirate_m1.png",
    });

    this.textures.addSpriteSheetFromAtlas("magratgarlick", {
      frameWidth: 32,
      frameHeight: 48,
      atlas: "json_atlas",
      frame: "magratgarlick.png",
    });

    this.textures.addSpriteSheetFromAtlas("pirate_m2", {
      frameWidth: 32,
      frameHeight: 48,
      atlas: "json_atlas",
      frame: "pirate_m2.png",
    });

    this.textures.addSpriteSheetFromAtlas("berserker_f", {
      frameWidth: 32,
      frameHeight: 51,
      atlas: "json_atlas",
      frame: "berserker_f.png",
    });

    this.textures.addSpriteSheetFromAtlas("blanca", {
      frameWidth: 48,
      frameHeight: 47.25,
      atlas: "json_atlas",
      frame: "blanca.png",
    });

    this.textures.addSpriteSheetFromAtlas("leviathan", {
      frameWidth: 96,
      frameHeight: 95.5,
      atlas: "json_atlas",
      frame: "leviathan.png",
    });

    this.textures.addSpriteSheetFromAtlas("phoenix", {
      frameWidth: 96,
      frameHeight: 95.5,
      atlas: "json_atlas",
      frame: "phoenix.png",
    });

    this.textures.addSpriteSheetFromAtlas("bahamut", {
      frameWidth: 96,
      frameHeight: 95.5,
      atlas: "json_atlas",
      frame: "bahamut.png",
    });

    this.textures.addSpriteSheetFromAtlas("bigboss", {
      frameWidth: 32,
      frameHeight: 48,
      atlas: "json_atlas",
      frame: "bigboss.png",
    });

    this.textures.addSpriteSheetFromAtlas("player", {
      frameWidth: 50,
      frameHeight: 85,
      atlas: "json_atlas",
      frame: "spritetest2.png",
    });

    // this.scene.start("testinventory-scene");
    // this.scene.start("testingground-scene");
    this.scene.start("intro-scene");
    // this.scene.start("town-scene");

    // this.scene.start("training-scene");
  }
}

export default PreloadScene;
