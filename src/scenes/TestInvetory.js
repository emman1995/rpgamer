import { Scene } from "phaser";

import { characterMovement, enemyPatrol, setGameUI } from "../functions/Global";

import cursorDefault from "../assets/cursor/tap.cur";
import cursorDefaultWalk from "../assets/cursor/tapTick.cur";

var player;
var enemy;
var cursors;
var destination = null;
var text;
var enemyDoneWalking = true;
var bar;
var pet;
var itemStorage;
var sampitem1;
var sampitem2;

var zone;
class TestInventory extends Scene {
  constructor() {
    super("testinventory-scene");
  }

  init(data) {
    console.log(data);
  }

  create() {
    var graphics = this.add.graphics();

    graphics.fillStyle(0x000000, 0);
    graphics.fillRect(0, 0, this.game.config.width, this.game.config.height);

    var inventoryArray = [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
    ];

    var demosWindow = this.add
      .image(
        this.game.config.width / 2,
        this.game.config.height / 2,
        "inventory"
      )
      .setOrigin(0, 0.5)
      .setInteractive();

    // var backgame = this.add
    //   .text(
    //     this.game.config.width / 2 + demosWindow.width / 2 + 10,
    //     this.game.config.height / 2 - demosWindow.height / 2 - 55,
    //     "Back",
    //     {
    //       fontFamily: '"Alagard"',
    //       fontSize: 28,
    //     }
    //   )
    //   .setInteractive();

    // backgame.on(
    //   "pointerup",
    //   function (pointer) {
    //     this.scene.stop();
    //     this.scene.resume("town-scene");
    //   },
    //   this
    // );

    // var sampitem = this.add.image(100, 150, "item_axe").setInteractive();
    // itemStorage = this.add.image(250, 150, "item_storage").setInteractive();

    sampitem1 = this.add.image(100, 200, "item_dagger").setInteractive();
    sampitem2 = this.add.image(100, 300, "item_axe").setInteractive();

    this.input.setDraggable(sampitem1);
    this.input.setDraggable(sampitem2);

    var d = 213;
    var b = 98;
    var count = 1;
    for (var c = 0; c < inventoryArray.length; c++) {
      for (var rows = 0; rows < inventoryArray[c].length; rows++) {
        var zone = this.add
          .zone(
            demosWindow.x - demosWindow.width / 2 + d,
            demosWindow.y - demosWindow.height / 2 + b,
            36,
            36
          )
          .setRectangleDropZone(36, 36);

        zone.customIdentifier = count;
        var graphics = this.add.graphics();
        graphics.lineStyle(2, "#fcba03", 1);
        graphics.strokeRect(
          zone.x - zone.input.hitArea.width / 2,
          zone.y - zone.input.hitArea.height / 2,
          zone.input.hitArea.width,
          zone.input.hitArea.height
        );

        d = d + 36;
        count++;
      }
      d = 213;
      b = b + 36;
    }

    this.dragAndDropFunction(sampitem2);
    this.dragAndDropFunction(sampitem1);
  }

  update() {}
  dragAndDropFunction(obj) {
    var sampleGraphics;
    var itemname;
    var itemdescription;

    obj.on("drag", function (pointer, dragX, dragY) {
      this.x = dragX;
      this.y = dragY;

      if (itemname && sampleGraphics && itemdescription) {
        // obj.setScale(1);
        sampleGraphics.destroy();
        itemname.destroy();
        itemdescription.destroy();

        itemname = null;
        sampleGraphics = null;
        itemdescription = null;
      }
    });

    obj.on("drop", function (pointer, dropZone) {
      dropZone.depth = -1;
      this.x = dropZone.x;
      this.y = dropZone.y;

      // this.setScale(0.8);
    });

    obj.on(
      "pointerover",
      function (pointer) {
        // obj.setScale(1.5);
        sampleGraphics = this.add.graphics();
        sampleGraphics.fillStyle(0x000000, 0.8);
        sampleGraphics.lineStyle(4, 0x000000, 0.8);
        sampleGraphics.fillRect(obj.x, obj.y, 200, 100);

        itemname = this.add.text(
          obj.x - obj.width / 2 + 70,
          obj.y - obj.height / 2 + 40,
          "Letter for Emman",
          {
            fontFamily: '"Alagard"',
            fontSize: 20,
          }
        );

        itemdescription = this.add.text(
          obj.x - obj.width / 2 + 50,
          obj.y - obj.height / 2 + 70,
          "- 500 Damage \n- 100 Attack Speed \n- 20 Mana",
          {
            fontFamily: '"Alagard"',
            fontSize: 15,
          }
        );
      },
      this
    );

    obj.on("pointerout", function (pointer) {
      // obj.setScale(1);
      if (itemname && sampleGraphics && itemdescription) {
        sampleGraphics.destroy();
        itemname.destroy();
        itemdescription.destroy();

        itemname = null;
        sampleGraphics = null;
        itemdescription = null;
      }
    });
  }
}

export default TestInventory;
