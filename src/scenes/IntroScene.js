import { Scene } from "phaser";

import { characterMovement, enemyPatrol, setGameUI } from "../functions/Global";

var player;
var enemy;
var cursors;
var destination = null;
var text;
var enemyDoneWalking = true;
var customenemy;

class IntroScene extends Scene {
  constructor() {
    super("intro-scene");
  }

  create() {
    var music = this.sound.add("intro");
    music.loop = true;
    music.play();

    console.log(this.game);

    var bg = this.add.image(
      this.game.config.width / 2,
      this.game.config.height / 2,
      "intro_bg"
    );
    bg.setDisplaySize(this.game.config.width, this.game.config.height);
    bg.setAlpha(0.7);

    var title = this.add
      .text(
        this.game.config.width / 2,
        this.game.config.height / 2 - 150,
        "Untitled.",
        {
          fontFamily: '"Alagard"',
          fontSize: 100,
        }
      )
      .setOrigin(0.5);

    var start = this.add
      .text(this.game.config.width / 2, this.game.config.height / 2, "Start", {
        fontFamily: '"Alagard"',
        fontSize: 50,
      })
      .setInteractive()
      .setOrigin(0.5);

    var option = this.add
      .text(
        this.game.config.width / 2,
        this.game.config.height / 2 + 80,
        "Testing Inventory System",
        {
          fontFamily: '"Alagard"',
          fontSize: 40,
        }
      )
      .setInteractive()
      .setOrigin(0.5);

    start.on(
      "pointerdown",
      function () {
        this.scene.start("town-scene");
        music.stop();
      },
      this
    );

    option.on(
      "pointerdown",
      function () {
        this.scene.start("testinventory-scene");
      },
      this
    );

    this.hoverEffect(start);
    this.hoverEffect(option);
  }

  update() {}

  hoverEffect(object) {
    object.on("pointerover", function () {
      object.setColor("#000");
    });

    object.on("pointerout", function () {
      object.setColor("#fff");
    });
  }
}

export default IntroScene;
